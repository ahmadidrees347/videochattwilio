package com.ai.twiliovideo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.camera2.CaptureRequest;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.twilio.video.AspectRatio;
import com.twilio.video.CameraCapturer;
import com.twilio.video.CameraParameterUpdater;
import com.twilio.video.CaptureRequestUpdater;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.LocalDataTrack;
import com.twilio.video.LocalParticipant;
import com.twilio.video.LocalVideoTrack;
import com.twilio.video.RemoteAudioTrack;
import com.twilio.video.RemoteAudioTrackPublication;
import com.twilio.video.RemoteDataTrack;
import com.twilio.video.RemoteDataTrackPublication;
import com.twilio.video.RemoteParticipant;
import com.twilio.video.RemoteVideoTrack;
import com.twilio.video.RemoteVideoTrackPublication;
import com.twilio.video.Room;
import com.twilio.video.ScreenCapturer;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoCapturer;
import com.twilio.video.VideoConstraints;
import com.twilio.video.VideoDimensions;
import com.twilio.video.VideoTrack;
import com.twilio.video.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.twilio.video.Room.State.CONNECTED;

public class MainActivity extends AppCompatActivity {

    EditText edt_room;
    Button btn_joinRoom;

    private static final String MICROPHONE_TRACK_NAME = "microphone";
    private Boolean isAudioMuted = false;
    private Boolean isVideoMuted = false;

    String accessToken = "";
    final String TAG = MainActivity.class.getSimpleName();

    Room room;
    LocalAudioTrack localAudioTrack;
    private LocalVideoTrack screenVideoTrack;
    CameraCapturer cameraCapturer;
    VideoView videoView, video_view_;
    LocalDataTrack localDataTrack;
    ImageButton localVideoImageButton;
    ImageButton localAudioImageButton;
    ImageButton disconnectButton, camera_switch_image_button;

    boolean enable = true;
    LocalParticipant localParticipant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        localVideoImageButton = findViewById(R.id.local_video_image_button);
        localVideoImageButton.setOnClickListener(v -> {
            isVideoMuted = !isVideoMuted;
            localVideoImageButton.setImageResource(
                    isVideoMuted == true
                            ? R.drawable.ic_videocam_white_24px
                            : R.drawable.ic_videocam_off_gray_24px);
        });
        localAudioImageButton = findViewById(R.id.local_audio_image_button);
        localAudioImageButton.setOnClickListener(v -> toggleLocalAudio());
        disconnectButton = findViewById(R.id.disconnect);
        disconnectButton.setOnClickListener(v -> disconnectButtonClick());
        camera_switch_image_button = findViewById(R.id.camera_switch_image_button);
        camera_switch_image_button.setOnClickListener(v -> cameraCapturer.switchCamera());

        getToken("lfz4x19wmujcknotvsiqb3ehr6d52pay");


        videoView = findViewById(R.id.video_view);

        video_view_ = findViewById(R.id.video_view_);

        // Setup video constraints
        VideoConstraints videoConstraints = new VideoConstraints.Builder()
                .aspectRatio(AspectRatio.ASPECT_RATIO_16_9)
                .minVideoDimensions(VideoDimensions.CIF_VIDEO_DIMENSIONS)
                .maxVideoDimensions(VideoDimensions.HD_720P_VIDEO_DIMENSIONS)
                .minFps(5)
                .maxFps(24)
                .build();
        cameraCapturer = new CameraCapturer(getApplicationContext(), CameraCapturer.CameraSource.FRONT_CAMERA);

        screenVideoTrack = LocalVideoTrack.create(getApplicationContext(), enable, cameraCapturer, videoConstraints);
        //screenVideoTrack = LocalVideoTrack.create(getApplicationContext(), enable, cameraCapturer, videoConstraints);
// If the constraints are not satisfied a null track will be returned
        if (screenVideoTrack == null) {
            Log.e(TAG, "Unable to satisfy constraints");
        }

        // Render a local video track to preview your camera
        screenVideoTrack.addRenderer(videoView);
        // Create an audio track
        localAudioTrack = LocalAudioTrack.create(getApplicationContext(), enable);
        localDataTrack = LocalDataTrack.create(getApplicationContext());

        edt_room = findViewById(R.id.edt_room);
        btn_joinRoom = findViewById(R.id.btn_joinRoom);
        btn_joinRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectToRoom(edt_room.getText().toString());
            }
        });

    }

    private void getToken(String room) {
        String url = "https://oranje-tech.com/demo/aptitudegym/api/join/" + room;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, response -> {
            Log.e("Res ", response);
            try {
                JSONObject jsonObject = new JSONObject(response);
                accessToken = jsonObject.getString("accessToken");
                edt_room.setText(jsonObject.getString("room"));

            } catch (JSONException e) {
            }
        }, error -> {
            if (error != null && error.networkResponse != null && error.networkResponse.data != null) {
                String str = new String(error.networkResponse.data);
                Log.e("Error : ", str);
                Toast.makeText(MainActivity.this, ""+str, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonObject = new JSONObject(str);
                    String strError = jsonObject.getString("error");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
                Log.e("Error : ", error.toString());
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> hashMap = new HashMap<>();
                //hashMap.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYWZiZTE1NGRiMzZjMTBlYTk2ZWQ4ZWViOThkZDRjNmMzYWZlZTJjMWZhNzI2YjA2YzdhNzBiZDE2ZTQ5YjVmMGY2MmE5MDVlYzNiZDVmZWUiLCJpYXQiOjE1ODg4NTA5ODcsIm5iZiI6MTU4ODg1MDk4NywiZXhwIjoxNjIwMzg2OTg3LCJzdWIiOiI3Iiwic2NvcGVzIjpbXX0.aaVlDReTVAyFoEOgMk0jpIKRaosFai51FWVzXyt3UfZxW_v1UrTbadhHMGadHuPNFKI5ZhC6GM8mdb25etxDX6aW5rp8BdbhIVrnmxQA1fOjy3LHgNm-zhBjh_Mzc4wG2T37JVywY7cZFqtygvSnQJfyNkFIjPB_NtrsuCEfuwHxZ4MsEvkihgbDaDpJNhWckvrMwUmPDdDnxr6hBysnAREwkjDQAIMjHgWyuYrCjlUmhFYqkQnECwIGTUPxmpkVh45zpCI0K18DArMz-B3PRJUcwxmS4GPIQVz7BAYkVJ8WLpn9_XVj70eMYuGeZCT0EwEWYTVektMU9e3aHSXOuEGz__0TxVySolIK51edej1hPl0rA5CGQhogVHCPDTmR-boPhbY7TkBlEpxgy-4dyR_yfV5mt78gZAfxsZp9NLOzja_G8GJnEe9aYzfPLoDW5vYJPGG-5WOVr-nJ6v2wms31DJmviOVNzLMuILuwjl77QPlFnt8JvUAH8VxgrBfND0rLKUQo4GpO1GQfD3ewAQfTadmb_FtNB1ro5Cm-Tl91hmshbrdJx-6XK_wr3tsgbc3OS6SMzG89rNTKF6v5_D9xjXBOGhKYE4RCUxCKoSZ47N-q-7J57vSFvmXxI8taTIJ-FgW3jq5mHp3ezg19y320z-UrPUp89TByYW3uODE");

                hashMap.put("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiN2RmYTdlOGRhZjI4YTA3NGMyNGQ1ZWZkMzhhZDk2YmJjODJlZGQwMTExNjVmNTdlMmE1OTllM2ZlMjY4N2MyYWViZTViNTA5NjY5N2I1NTciLCJpYXQiOjE1ODg4MzgyNTQsIm5iZiI6MTU4ODgzODI1NCwiZXhwIjoxNjIwMzc0MjU0LCJzdWIiOiIyMyIsInNjb3BlcyI6W119.iRqppC0wjFafUn28t6zkv1T7OU-3OQKiaGbGbRr8cCdn4uMRnZWpSXuIIEoFEYYJpFt8yLsKf6dlmFeTr1JvQv-70byklj5p3mmFmm7o-DpV1d6lIuROvl0i_mKKZCvNMNXGMYfOT-hTsCpNGXOAh8MxgGSmwksAVtZYZdEiYph4pTmW_mVlXf4BJjsrPHZ7TzR9Xz3aG0Rc2fOtIqyGqbPX1AMesMY4GcPHs-T9iNcpZYFI11alu8sd0Y06JuABq_tTckcvAMw9M6z4JPiceGiYCz-frXQcpkYUZ4X6kuWccsyShjfctt9NBOHY-GlaExdl6qaG_LPtQT8dlAGDINOWfGNS5bN9E7dejBGVS11nDoSwLjJYljZyBsHwkHpXyQbMlJmerZbmtRVh6cPriNvWe6UyxE-syDn3zYHWDYlKfuI70YHCP356LR_QS95HipJtU67ZHm0OtOvRyRCsjNQfx2Xsg1fNdRzN5vixoRUuMQC39tz7u-vcnZFhqMQAH9VJTleSPNGhR1Q_jbdUMEeep-vdMOsOIvtWaXnjsox1HvIxQKV9pwbe1WzWiru_ZLNAA5woHLQpdS1on5bv-MIdjuietWNX521V80JlWt30xZeZb8xHCNApch9AcGcTIjhv6H8dGqFQPLFKLKxjGL5fdaHgT-tZ1hWYyoEaThw");
                hashMap.put("Accept","application/json");
                return hashMap;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    void disconnectButtonClick() {
        room.disconnect();
    }


    public void connectToRoom(String roomName) {


        /*for(int i=0;i<Collections.singletonList(screenVideoTrack).size();i++) {
            Log.e(TAG, Collections.singletonList(screenVideoTrack).get(i).getName());
        }*/

        ConnectOptions connectOptions = new ConnectOptions.Builder(accessToken)
                .roomName(roomName)
                .audioTracks(Collections.singletonList(localAudioTrack))
                .videoTracks(Collections.singletonList(screenVideoTrack))
                .dataTracks(Collections.singletonList(localDataTrack))
                .build();
        room = Video.connect(getApplicationContext(), connectOptions, roomListener());
        localParticipant = room.getLocalParticipant();
    }
    private Room.Listener roomListener() {
        return new Room.Listener() {
            @Override
            public void onConnected(Room room) {
                Log.e(TAG, "Connected to " + room.getName());

                localParticipant = room.getLocalParticipant();
                localParticipant.publishTrack(localDataTrack);
                localParticipant.publishTrack(screenVideoTrack);
                localParticipant.publishTrack(localAudioTrack);
            }

            @Override
            public void onConnectFailure(@NonNull Room room, @NonNull TwilioException twilioException) {
                Log.e(TAG, "Failed to Connected " + room.getName());
            }

            @Override
            public void onReconnecting(@NonNull Room room, @NonNull TwilioException twilioException) {
                Log.e(TAG, "onReconnecting to " + room.getName());
            }

            @Override
            public void onReconnected(@NonNull Room room) {
                Log.e(TAG, "onReconnected to " + room.getName());
            }

            @Override
            public void onDisconnected(@NonNull Room room, @Nullable TwilioException twilioException) {
                Log.e(TAG, "onDisconnected to " + room.getName());
            }

            @Override
            public void onParticipantConnected(@NonNull Room room, @NonNull RemoteParticipant remoteParticipant) {
                Log.e(TAG, remoteParticipant.getIdentity() + " has joined the room.");
                remoteParticipant.setListener(participantListener());

                Log.e(TAG, "Track Size " + remoteParticipant.getVideoTracks().size());
                /*if (remoteParticipant.getVideoTracks().size() == 0) {
                    CameraCapturer cameraCapturer = new CameraCapturer(getApplicationContext(), CameraCapturer.CameraSource.FRONT_CAMERA);
                    LocalVideoTrack localVideoTrack = LocalVideoTrack.create(getApplicationContext(), true, cameraCapturer);
                }*/
            }

            @Override
            public void onParticipantDisconnected(@NonNull Room room, @NonNull RemoteParticipant remoteParticipant) {
                Log.i("Room.Listener", remoteParticipant.getIdentity() + " has left the room.");
                remoteParticipant.getVideoTracks().size();
            }

            @Override
            public void onRecordingStarted(@NonNull Room room) {
                Log.e(TAG, "onRecordingStarted to " + room.getName());
            }

            @Override
            public void onRecordingStopped(@NonNull Room room) {
                Log.e(TAG, "onRecordingStopped to " + room.getName());
            }

        };
    }

    private RemoteParticipant.Listener participantListener() {
        return new RemoteParticipant.Listener() {
            @Override
            public void onAudioTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.e(TAG, "onAudioTrackPublished");
            }

            @Override
            public void onAudioTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.e(TAG, "onAudioTrackUnpublished");
            }

            @Override
            public void onAudioTrackSubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull RemoteAudioTrack remoteAudioTrack) {
                Log.e(TAG, "onAudioTrackSubscribed");
            }

            @Override
            public void onAudioTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull TwilioException twilioException) {
                Log.e(TAG, "onAudioTrackSubscriptionFailed");
            }

            @Override
            public void onAudioTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication, @NonNull RemoteAudioTrack remoteAudioTrack) {
                Log.e(TAG, "onAudioTrackUnsubscribed");
            }

            @Override
            public void onVideoTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.e(TAG, "onVideoTrackPublished");
            }

            @Override
            public void onVideoTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.e(TAG, "onVideoTrackUnpublished");
            }

            @Override
            public void onVideoTrackSubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication, @NonNull RemoteVideoTrack remoteVideoTrack) {
                Log.e(TAG, "onVideoTrackSubscribed");
                video_view_.setMirror(false);
                remoteVideoTrack.addRenderer(video_view_);
            }

            @Override
            public void onVideoTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication, @NonNull TwilioException twilioException) {
                Log.e(TAG, "onVideoTrackSubscriptionFailed "+twilioException.getExplanation()+", "+twilioException.getMessage());
            }

            @Override
            public void onVideoTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication, @NonNull RemoteVideoTrack remoteVideoTrack) {
                Log.e(TAG, "onVideoTrackUnsubscribed");
            }

            @Override
            public void onDataTrackPublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication) {
                Log.e(TAG, "onDataTrackPublished");
            }

            @Override
            public void onDataTrackUnpublished(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication) {
                Log.e(TAG, "onDataTrackUnpublished");
            }

            @Override
            public void onDataTrackSubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull RemoteDataTrack remoteDataTrack) {
                Log.e(TAG, "onDataTrackSubscribed");
            }

            @Override
            public void onDataTrackSubscriptionFailed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull TwilioException twilioException) {
                Log.e(TAG, "onDataTrackSubscriptionFailed");
            }

            @Override
            public void onDataTrackUnsubscribed(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteDataTrackPublication remoteDataTrackPublication, @NonNull RemoteDataTrack remoteDataTrack) {
                Log.e(TAG, "onDataTrackUnsubscribed");
            }

            @Override
            public void onAudioTrackEnabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.e(TAG, "onAudioTrackEnabled");
            }

            @Override
            public void onAudioTrackDisabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteAudioTrackPublication remoteAudioTrackPublication) {
                Log.e(TAG, "onAudioTrackDisabled");
            }

            @Override
            public void onVideoTrackEnabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.e(TAG, "onVideoTrackEnabled");
            }

            @Override
            public void onVideoTrackDisabled(@NonNull RemoteParticipant remoteParticipant, @NonNull RemoteVideoTrackPublication remoteVideoTrackPublication) {
                Log.e(TAG, "onVideoTrackDisabled");
            }
        };
    }


    @Override
    protected void onDestroy() {

        // Release the audio track to free native memory resources
        if (localAudioTrack != null) {
            localAudioTrack.release();
            localAudioTrack = null;
        }
        /*if (cameraVideoTrack != null) {
            cameraVideoTrack.release();
            cameraVideoTrack = null;
        }*/
        // Release the video track to free native memory resources
        if (screenVideoTrack != null) {
            screenVideoTrack.release();
            screenVideoTrack = null;
        }

        super.onDestroy();
    }


    void toggleLocalAudio() {
        int icon;
        if (localAudioTrack == null) {
            isAudioMuted = false;
            localAudioTrack = LocalAudioTrack.create(this, true, MICROPHONE_TRACK_NAME);
            if (localParticipant != null && localAudioTrack != null) {
                localParticipant.publishTrack(localAudioTrack);
            }
           icon = R.drawable.ic_mic_white_24px;
        } else {
            isAudioMuted = true;
            if (localParticipant != null) {
                localParticipant.unpublishTrack(localAudioTrack);
            }
            localAudioTrack.release();
            localAudioTrack = null;
            icon = R.drawable.ic_mic_off_gray_24px;
        }
        localAudioImageButton.setImageResource(icon);
    }
}
